namespace tech_test_payment_api.Models
{
    public enum EnumStatusVenda
    {
        Pagamento_aprovado,
        Enviado_para_transportadora,
        Entregue,
        Cancelada

    }
}